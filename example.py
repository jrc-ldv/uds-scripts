#!/usr/bin/env python3

import pandas as pd

from src.uds4jrc.config import Config
from src.uds4jrc.db import eea_raw_data
from src.uds4jrc.utils import save_to_parquet

df = pd.DataFrame.from_records(eea_raw_data.find({}, {'_id': 0}).limit(10))

...  # process your data

save_to_parquet(df, Config.UEOS, 'test.parquet')
