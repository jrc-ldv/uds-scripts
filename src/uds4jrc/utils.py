from os import PathLike
from pathlib import Path


def save_to_parquet(dataframe, folder: PathLike, filename, **kw):
    folder = Path(folder)
    folder.mkdir(parents=True, exist_ok=True)

    path = folder / filename
    dataframe.to_parquet(path, **kw)
