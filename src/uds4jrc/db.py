from pymongo import MongoClient

from .config import Config

client = MongoClient(
    username=Config.DB_USERNAME,
    password=Config.DB_PASSWORD,
    host=Config.DB_HOST,
    port=Config.DB_PORT,
    authSource=Config.DB_AUTH_SOURCE
)

db = client["legent_db"]

# collections definitions
eea_raw_data = db["eea_raw_data"]
fiat_data = db["fiat_500_data"]
geco_data = db["geco_air_data"]
spritmonitor_data = db["spritmonitor_data"]
travelcard_data = db["travelcard_data"]
atct_data = db["atct_data"]
cop_data = db["cop_data"]
obfcm_data = db["obfcm_data"]
jrcmatics_data = db["jrcmatics_data"]

# reference collections definitions
eea_reference = db["eea_properties_reference"]
fiat_reference = db["fiat_500_properties_reference"]
geco_reference = db["geco_air_properties_reference"]
travelcard_reference = db["travelcard_properties_reference"]
spritmonitor_reference = db["spritmonitor_properties_reference"]
atct_reference = db["atct_properties_reference"]
cop_reference = db["cop_properties_reference"]
obfcm_reference = db["obfcm_properties_reference"]
jrcmatics_reference = db["jrcmatics_properties_reference"]

# EEA views definitions
eea_2013_flattened = db["eea_2013_flattened"]
eea_2014_flattened = db["eea_2014_flattened"]
eea_2015_flattened = db["eea_2015_flattened"]
eea_2016_flattened = db["eea_2016_flattened"]
eea_2017_flattened = db["eea_2017_flattened"]
eea_2018_flattened = db["eea_2018_flattened"]
eea_2019_flattened = db["eea_2019_flattened"]
eea_2020_flattened = db["eea_2020_flattened"]
