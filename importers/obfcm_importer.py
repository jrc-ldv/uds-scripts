#!/usr/bin/env/ python

from collections import defaultdict

import pandas as pd
from munch import Munch

from src.uds4jrc.config import Config
from src.uds4jrc.db import obfcm_data, obfcm_reference

file_config = Munch(Config.HDV_LABS)


def _import_data():
    df = pd.read_excel(
        file_config.file_path,
        header=0,
        dtype=defaultdict(
            lambda: object, file_config.column_properties.iloc[:, 1:3].values),
        names=file_config.column_properties.iloc[:, 0].values,
        skiprows=file_config.skip_rows
    )

    df['file_year'] = file_config.year
    df['data'] = file_config.data
    df['version'] = 1.0

    obfcm_data.insert_many(df.to_dict('records'))


def _import_reference_data():
    df = file_config.column_properties
    reference_docs = []

    for i in range(len(df)):
        reference_docs.append({
            'db_property_name': df.iloc[i, 0],
            'file_property_name': df.iloc[i, 1],
            'file_year': file_config.year,
            'data': file_config.data
        })

    obfcm_reference.insert_many(reference_docs)


_import_data()
_import_reference_data()
