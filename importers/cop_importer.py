#!/usr/bin/env/ python

from collections import defaultdict

import pandas as pd
from munch import Munch

from src.uds4jrc.config import Config
from src.uds4jrc.db import cop_data, cop_reference

file_config = Munch(Config.COP_NED)


def _import_data():
    df = pd.read_excel(
        file_config.file_path,
        header=0,
        dtype=defaultdict(
            lambda: object, file_config.column_properties.iloc[:, 1:3].values),
        names=file_config.column_properties.iloc[:, 0].values,
        skiprows=file_config.skip_rows
    )

    df.dropna(how='all', inplace=True)

    df['data'] = file_config.data
    df['version'] = 1.0

    cop_data.insert_many(df.to_dict('records'))


def _import_reference_data():
    df = file_config.column_properties
    reference_docs = []

    for i in range(len(df)):
        reference_docs.append({
            'db_property_name': df.iloc[i, 0],
            'file_property_name': df.iloc[i, 1],
            'data': file_config.data
        })

    cop_reference.insert_many(reference_docs)


_import_data()
_import_reference_data()
