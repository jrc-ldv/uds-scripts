import datetime
import uuid
import multiprocessing as mp
import pandas as pd

from munch import Munch
from src.uds4jrc.db import jrcmatics_data
from bson.binary import Binary, UuidRepresentation


def update_trip_with_jump(trip):
    trip = Munch(trip)
    trip_guid_binary = Binary.from_uuid(trip.trip_guid, UuidRepresentation.STANDARD)

    delta = datetime.timedelta(seconds=0)

    timestamp_anchor = 0
    prev_doc = None
    jump_without_downtime = False

    time_anchor_point = trip.trip_created_at

    rs = list(jrcmatics_data.find(
        {
            'trip_guid': trip_guid_binary
        },
        {
            '_id': 1,
            'timestamp_corr': 1,
            'trip_created_at': 1,
            'created_at': 1,
            'time': 1,
            'time_corr': 1
        }
    ))

    sorted_rs = sorted(rs, key=lambda k: k['created_at'])

    df = pd.DataFrame(sorted_rs)

    for doc in sorted_rs:
        doc = Munch(doc)

        if timestamp_anchor == 0:
            timestamp_anchor = int(doc.timestamp_corr)

        # if the check is True, it means that the device has reset
        if prev_doc and abs(int(doc.timestamp_corr / 1000) - int(prev_doc.timestamp_corr / 1000)) > 180:
            reset_downtime = (doc.created_at - prev_doc.created_at).total_seconds() * 1000

            if int(doc.timestamp_corr) - timestamp_anchor < 0:
                if reset_downtime:
                    delta = datetime.timedelta(milliseconds=reset_downtime)
                    timestamp_anchor = int(doc.timestamp_corr)
                else:
                    jump_without_downtime = True
                    timestamp_anchor = int(prev_doc.timestamp_corr + doc.timestamp_corr)

            time_anchor_point = prev_doc.time + delta

        current_timestamp = int(timestamp_anchor + doc.timestamp_corr) if jump_without_downtime else int(doc.timestamp_corr)
        time = time_anchor_point + datetime.timedelta(milliseconds=current_timestamp) - datetime.timedelta(milliseconds=timestamp_anchor)

        jrcmatics_data.update_one(
            {
                '_id': doc._id
            },
            {
                '$set': {
                    'time_corr': time
                }
            }
        )

        doc.time = time
        prev_doc = doc

    jrcmatics_data.update_many(
        {
            'trip_guid': trip_guid_binary
        },
        {
            '$set': {
                'jump_updated': True
            }
        }
    )

    print(f"time_corr values generated for trip {str(trip.trip_guid)}")


if __name__ == "__main__":
    guid = uuid.UUID('eb02bd4571cb4b5da0b4b9c0a10e9ad0')
    trip_guid_binary = Binary.from_uuid(guid, UuidRepresentation.STANDARD)

    trips = list(jrcmatics_data.aggregate([
        {"$match": {
            'trip_guid': trip_guid_binary
        }},
        {"$group": {
            "_id": {"trip_guid": "$trip_guid"},
            "trip_created_at": {"$first": "$trip_created_at"}
        }},
        {"$project": {
            "_id": 0,
            "trip_guid": '$_id.trip_guid',
            "trip_created_at": "$trip_created_at"
        }}
    ]))

    p = mp.Pool(1)
    p.map(update_trip_with_jump, trips)
