#!/usr/bin/env python3

tvin = 'JM4BP6HE601165733'  # A0HNZZ33
dfrom = 'Oct 01 2022'
dto = 'Nov 08 2022'

import datetime
import dateparser
import pandas
from src.uds4jrc.db import jrcmatics_data

list_df = []

for vin in jrcmatics_data.find({}).distinct('vin'):

    # if( vin != tvin ): continue

    pipeline = \
        [
            {
                '$match': {
                    'vin': vin,
                    'trip_created_at': {'$gt': dateparser.parse(dfrom),
                                        '$lt': dateparser.parse(dto) + datetime.timedelta(days=1)}
                }
            }, {
            '$sort': {
                'trip_created_at': 1,
                'time': 1,
            }
        }, {
            '$group': {
                '_id': '$trip_created_at',
                'vin': {'$first': '$vin'},
                'dev_id': {'$first': '$dev_id'},
                'trip_guid': {'$first': '$trip_guid'},
                'min_time': {'$min': '$time'},
                'max_time': {'$max': '$time'},
                'tot_distances': {'$push': '$tot_distance'},
                'max_tot_distance': {'$max': '$tot_distance'},
                'tot_fuels': {'$push': '$tot_fuel'},
                'max_tot_fuel': {'$max': '$tot_fuel'},
                'min_ambient_temp': {'$min': '$ambient_temp'},
                'max_ambient_temp': {'$max': '$ambient_temp'},
                'mean_ambient_temp': {'$avg': '$ambient_temp'},
                'min_distance_cd_ice_on': {'$min': '$distance_cd_ice_on'},
                'max_distance_cd_ice_on': {'$max': '$distance_cd_ice_on'},
                'min_distance_cd_ice_off': {'$min': '$distance_cd_ice_off'},
                'max_distance_cd_ice_off': {'$max': '$distance_cd_ice_off'},
                'mean_speed': {'$avg': '$speed'},
                'std_speed': {'$stdDevPop': '$speed'},
                'mean_speed_rural': {
                    '$avg': {
                        '$cond': [{'$and': [{'$gte': ['$speed', 0.0]}, {'$lte': ['$speed', 60.0]}]}, '$speed', None]}},
                'mean_speed_urban': {
                    '$avg': {
                        '$cond': [{'$and': [{'$gt': ['$speed', 60.0]}, {'$lte': ['$speed', 90.0]}]}, '$speed', None]}},
                'mean_speed_motorway': {'$avg': {
                    '$cond': [{'$and': [{'$gt': ['$speed', 90.0]}, {'$lte': ['$speed', 200.0]}]}, '$speed', None]}},
                'stop_percentage': {'$avg': {'$cond': [{'$eq': ['$speed', 0.0]}, 1.0, 0.0]}},
                'mean_rpm': {'$avg': '$rpm'},
                'std_rpm': {'$stdDevPop': '$rpm'},
                'min_fuel_cd': {'$min': '$fuel_cd'},
                'max_fuel_cd': {'$max': '$fuel_cd'},
                'min_fuel_ci': {'$min': '$fuel_ci'},
                'max_fuel_ci': {'$max': '$fuel_ci'},
                'coolant_temp_arr': {'$push': '$coolant_temp'},
                'urban_share': {'$avg': {
                    '$cond': [
                        {'$and': [{'$ne': ['$speed', None]}, {'$gte': ['$speed', 0.0]}, {'$lte': ['$speed', 60.0]}]},
                        1.0, 0.0]}},
                'rural_share': {'$avg': {
                    '$cond': [
                        {'$and': [{'$ne': ['$speed', None]}, {'$gt': ['$speed', 60.0]}, {'$lte': ['$speed', 90.0]}]},
                        1.0, 0.0]}},
                'motorway_share': {'$avg': {
                    '$cond': [
                        {'$and': [{'$ne': ['$speed', None]}, {'$gt': ['$speed', 90.0]}, {'$lte': ['$speed', 200.0]}]},
                        1.0, 0.0]}},
                'mean_acc_pedal_pos_d': {'$avg': '$acc_pedal_pos_d'},
                'std_acc_pedal_pos_d': {'$stdDevPop': '$acc_pedal_pos_d'},
                'grid_energy_cd_ice_off_arr': {'$push': '$grid_energy_cd_ice_off'},
                'mean_grid_energy_cd_ice_off': {'$avg': '$grid_energy_cd_ice_off'},
                'grid_energy_cd_ice_on_arr': {'$push': '$grid_energy_cd_ice_on'},
                'mean_grid_energy_cd_ice_on': {'$avg': '$grid_energy_cd_ice_on'},
                'grid_energy_into_bat_arr': {'$push': '$grid_energy_into_bat'},
                'mean_grid_energy_into_bat': {'$avg': '$grid_energy_into_bat'},
            }
        }, {
            '$addFields': {
                'min_tot_distance': {'$ifNull': [
                    {
                        '$min': {
                            '$filter': {
                                'input': "$tot_distances",
                                'cond': {'$gt': ['$$this', 0]}
                            }
                        }
                    },
                    0
                ]},
                'min_tot_fuel': {'$ifNull': [
                    {
                        '$min': {
                            '$filter': {
                                'input': "$tot_fuels",
                                'cond': {'$gt': ['$$this', 0]}
                            }
                        }
                    },
                    0
                ]},
            }
        }, {
            '$addFields': {
                'distance': {'$subtract': ['$max_tot_distance', '$min_tot_distance']},
                'fuel': {'$subtract': ['$max_tot_fuel', '$min_tot_fuel']},
                'duration': {'$divide': [{'$subtract': ['$max_time', '$min_time']}, 1000]},
                'distance_fuel_on': {'$subtract': ['$max_distance_cd_ice_on', '$min_distance_cd_ice_on']},
                'distance_fuel_off': {'$subtract': ['$max_distance_cd_ice_off', '$min_distance_cd_ice_off']},
                'fuel_ci': {'$subtract': ['$max_fuel_ci', '$min_fuel_ci']},
                'fuel_cd': {'$subtract': ['$max_fuel_cd', '$min_fuel_cd']},
                'start_coolant_temp': {'$arrayElemAt': ['$coolant_temp_arr', 0]},
                'start_grid_energy_cd_ice_off': {'$arrayElemAt': ['$grid_energy_cd_ice_off_arr', 0]},
                'end_grid_energy_cd_ice_off': {'$arrayElemAt': ['$grid_energy_cd_ice_off_arr', -1]},
                'start_grid_energy_cd_ice_on': {'$arrayElemAt': ['$grid_energy_cd_ice_on_arr', 0]},
                'end_grid_energy_cd_ice_on': {'$arrayElemAt': ['$grid_energy_cd_ice_on_arr', -1]},
                'start_grid_energy_into_bat': {'$arrayElemAt': ['$grid_energy_into_bat_arr', 0]},
                'end_grid_energy_into_bat': {'$arrayElemAt': ['$grid_energy_into_bat_arr', -1]},
            }
        }, {
            '$match': {'distance': {'$gt': 0}, 'duration': {'$gt': 0}}
        }, {
            '$sort': {
                '_id': -1
            }
        }, {
            '$project': {
                # '_id': 0,
                'min_distance_cd_ice_on': 0,
                'max_distance_cd_ice_on': 0,
                'min_distance_cd_ice_off': 0,
                'max_distance_cd_ice_off': 0,
                'min_fuel_cd': 0,
                'max_fuel_cd': 0,
                'min_fuel_ci': 0,
                'max_fuel_ci': 0,
                'coolant_temp_arr': 0,
                'grid_energy_cd_ice_off_arr': 0,
                'grid_energy_cd_ice_on_arr': 0,
                'grid_energy_into_bat_arr': 0,
            }
        }
        ]

    results_df = pandas.DataFrame(list(jrcmatics_data.aggregate(pipeline, allowDiskUse=True)))

    if results_df.empty:
        continue
    else:
        list_df.append(results_df)

final_df = pandas.concat(list_df, ignore_index=True)

final_df.to_excel("/home/vliagth/aggregated_trips.xlsx", index=False)
final_df.to_csv("/home/vliagth/aggregated_trips.csv", index=False)
