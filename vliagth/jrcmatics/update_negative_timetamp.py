#!/usr/bin/env python3

from munch import Munch
from src.uds4jrc.db import jrcmatics_data

ABS_MIN_INT_32 = 2147483648

# copy values of positive timestamp in new field
jrcmatics_data.update_many(
    {
        "timestamp": {"$gte": 0}
    },
    [{
        "$set": {
            "timestamp_corr": "$timestamp"
        }
    }]
)

# find and update the negative timestamp values using the newly generated field
docs = jrcmatics_data.find(
    {
        'timestamp': {'$lt': 0}
    },
    {
        '_id': 1,
        'timestamp': 1
    }
)

for doc in docs:
    doc = Munch(doc)

    jrcmatics_data.update_one(
        {
            '_id': doc._id
        },
        {
            '$set': {
                'timestamp_corr': 2 * ABS_MIN_INT_32 + doc.timestamp,
                'had_negative_timestamp': True
            }
        }
    )

