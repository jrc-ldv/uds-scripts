#!/usr/bin/env python3

import multiprocessing as mp
import uuid

from bson.binary import Binary, UuidRepresentation
from munch import munchify
from src.uds4jrc.db import jrcmatics_data


def set_trip_guid(trip):
    trip = munchify(trip)

    guid = uuid.uuid4()
    guid_binary = Binary.from_uuid(guid, UuidRepresentation.STANDARD)

    jrcmatics_data.update_many(
        {
            'dev_id': trip.device,
            'trip_created_at': trip.trip_created_at
        },
        {
            '$set': {
                'trip_guid': guid_binary
            }
        }
    )

    print(f"Updated trip. Dev id: {trip.device} created at {trip.trip_created_at}")


if __name__ == "__main__":
    trips = list(jrcmatics_data.aggregate([
        {"$match":
             {'trip_guid': {"$exists": False}}
         },
        {"$group":
            {"_id": {
                "device": "$dev_id",
                "trip_created_at": "$trip_created_at"
            }}
        },
        {"$project": {
            "_id": 0,
            "device": '$_id.device',
            "trip_created_at": "$_id.trip_created_at"
        }}
    ]))

    p = mp.Pool(100)
    p.map(set_trip_guid, trips)


# Example
# www.jrcmatics.com/device/A0HLZXHE/trip/4c0cef49-4442-4499-ab3f-77f329875cb4
# www.jrcmatics.com/trip/4c0cef49-4442-4499-ab3f-77f329875cb4
# www.jrcmatics.com/device/A0HLZXHE/trip/20220208-104803
