#!/usr/bin/env python3

import multiprocessing as mp
import pandas as pd
import uuid

from bson.binary import Binary, UuidRepresentation
from munch import munchify
from datetime import datetime
from src.uds4jrc.db import jrcmatics_data


if __name__ == "__main__":
    csv_trips = pd.read_csv(
        '/eos/jeodpp/data/projects/LEGENT/internal/jrcmatics/jrcmatics_trips.csv',
        delimiter=',',
        header=0,
        encoding='utf-8',
        index_col=False
    )

    csv_trips.created_at = pd.to_datetime(csv_trips.created_at)
    # csv_trips = csv_trips.drop(columns=['guid'])

    db_trips = pd.DataFrame(list(jrcmatics_data.aggregate([
        {"$match":
             {'trip_guid': {"$exists": False}}
        },
        {"$group":
            {"_id": {
                "device": "$dev_id",
                "trip_created_at": "$trip_created_at"
            }}
        },
        {"$project": {
            "_id": 0,
            "device": '$_id.device',
            "trip_created_at": "$_id.trip_created_at"
        }}
    ])))

    s1 = pd.merge(db_trips, csv_trips, how='inner', on=['device', 'trip_created_at'])

    test = 2




# Example
# www.jrcmatics.com/device/A0HLZXHE/trip/4c0cef49-4442-4499-ab3f-77f329875cb4
# www.jrcmatics.com/trip/4c0cef49-4442-4499-ab3f-77f329875cb4
# www.jrcmatics.com/device/A0HLZXHE/trip/20220208-104803
