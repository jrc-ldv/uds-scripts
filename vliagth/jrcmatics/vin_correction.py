from src.uds4jrc.db import jrcmatics_data
import pandas as pd
import numpy as np

vin_correction_filepath = "/eos/jeodpp/home/users/tansial/vin_correction.xlsx"

for vin in jrcmatics_data.distinct('vin'):
    print(vin)
    x = jrcmatics_data.update_many({"vin": {"$eq":vin}}, {"$set": {"vin_corr": vin}})
    print(x.modified_count, " documents copied.\n")

vin_correction = pd.read_excel(vin_correction_filepath)

dev_ids = vin_correction['dev_id'].values

list_to_get = ['time', 'dev_id', 'trip_created_at', 'vin']
select_cl = {key: 1 for key in list_to_get}

data = {}

for dev_id in dev_ids:
    rawData = jrcmatics_data.find({'dev_id': dev_id}, select_cl)
    data[dev_id] = pd.DataFrame(rawData)

for idx, row in vin_correction.iterrows():
    dev_id = row.dev_id
    print("ID: ", dev_id)
    if not data[dev_id].empty:
        if 'vin' in data[dev_id].columns:
            vin = row.vin
            start = pd.to_datetime(np.datetime64(row.date_start))
            end = pd.to_datetime((np.datetime64(row.date_end) if pd.notnull(row.date_end) else np.datetime64('now')) + np.timedelta64(1, 'D'))
            
            vin_missing = pd.isnull(data[dev_id]['vin'])
            dates_ok = (data[dev_id]['trip_created_at'] > start) & (data[dev_id]['trip_created_at'] < end)
            select = vin_missing & dates_ok
            
            ids = list(data[dev_id][select]['_id'].values)
            
            where_cl = {
                "_id": {"$in": ids},
            }
            update_cl = {"$set": {"vin_corr": vin}}
            x = jrcmatics_data.update_many(where_cl, update_cl)
            print(x.modified_count, " documents updated.")
        else:
            print("VIN not available, skipping.")
        print("\n")