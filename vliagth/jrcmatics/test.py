#!/usr/bin/env python3

import multiprocessing as mp
import pandas as pd
import uuid

from bson.binary import Binary, UuidRepresentation
from munch import munchify
from datetime import datetime
from src.uds4jrc.db import jrcmatics_data


if __name__ == "__main__":
    guid = uuid.UUID('f57684b13b4646438fc881a142e7dddf')
    trip_guid_binary = Binary.from_uuid(guid, UuidRepresentation.STANDARD)

    trip_created_at = datetime.strptime('2021-11-14 14:56:31', '%Y-%m-%d %H:%M:%S')

    rs = pd.DataFrame(list(jrcmatics_data.find(
        {
            'dev_id': 'A0HL8M13',
            'trip_created_at': trip_created_at
        },
        {
            '_id': 1,
            'trip_guid': 1,
            'timestamp_corr': 1,
            'trip_created_at': 1,
            'created_at': 1,
            'time': 1,
            'time_corr': 1,
        }
    )))

    test = 3




# Example
# www.jrcmatics.com/device/A0HLZXHE/trip/4c0cef49-4442-4499-ab3f-77f329875cb4
# www.jrcmatics.com/trip/4c0cef49-4442-4499-ab3f-77f329875cb4
# www.jrcmatics.com/device/A0HLZXHE/trip/20220208-104803
