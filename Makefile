.PHONY: all setup create_conda_env rm_conda_env
BASH := /bin/bash
MYDIR := $(dir $(abspath $(firstword $(MAKEFILE_LIST))))
conda_env_prefix := $(MYDIR)/.env

all: setup

setup: ~/.inputrc ~/.bashrc ~/.bash_aliases ~/.gitconfig create_conda_env

~/.inputrc: setup-account.sh
	@./$< upd20_inputrc
	@touch $@

~/.bashrc: setup-account.sh
	@./$< upd20_bashrc_history
	@./$< upd20_bashrc_git_completions
	@./$< upd20_bashrc_ustore_vars
	@./$< upd40_bashrc_conda_init
	@touch $@

~/.bash_aliases: setup-account.sh
	@./$< upd20_bash_aliases
	@touch $@

~/.ipython/profile_default/startup/50-ustor-vars.py: setup-account.sh
	@./$< upd20_ipython_ustore_vars
	@touch $@

~/.gitconfig: setup-account.sh
	@./$< upd20_git_aliases
	@./$< upd30_git_user
	@touch $@

create_conda_env: $(conda_env_prefix)

$(conda_env_prefix): setup-account.sh environment.yml ~/.bashrc
	@./$< upd50_create_conda_env

upd_conda_env:
	conda env update --prefix $(conda_env_prefix) --file environment.yml --prune

## FIXME: canot work from within env, but `make` is missing outside (see #834)
rm_conda_env:
	conda env remove --prefix $(conda_env_prefix)


# Command to print all the other targets, from https://stackoverflow.com/a/26339924
help:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'
