#!/bin/bash
#
# Call this script with the name of the action-function as arg[1].
function _HELP { echo \
"$mycmd - setup a BDAP user account with (pending) actions for keyboard, bash, git, conda
logout & re-login:
    $mycmd [-h]--help] ...
    $mycmd [-n] [-k] [-f] [ACTION]...
OPTIONS:
    -n:  dry_run    - don't actually update stuff (but evaluate checks)
    -k:  keep_going - don't stop on errors
    -f:  force      - update stuff regardless of checks
ACTION:
$(for i in $(_list_all_actions); do echo "    $i"; done)
NOTES:
    * Without any args, it  launches 'all' actions above, in that order.
    * The 'upd30_git_user_XXX' actions awaits interactively input from the user.
    * It stops after 'upd40_bashrc_conda_init', to logout & re-login,
      for 'upd50_create_conda_env' to succeed (unless -k).
    * You can repeatedly run this script, until all actions succeed.
"
}

mycmd="${0##*/}"
mydir=$(realpath "${0%/*}")
cd "$mydir"

function _short {
    ## Return vars relative to some base(default: PWD),
    #  prefixed with "./", as `conda activate <prefix>` needs it.
    local base="${2:-$PWD}"
    local path=$(realpath -m --relative-base="$base" "$1")
    ## Does result start with alpha-chars (eg dirname)?.
    if [[ "/." != *"${path::1}"* ]]; then
        echo "./$path"
    else
        echo "$path"
    fi
}

function _homed {
    ## Return vars relative to home-dir,
    #  prefixed with "~/", as `conda activate <prefix>` needs it.
    local path=$(realpath -m --relative-base="$PWD" "$1")
    ## Does result start with alpha-chars (eg dirname)?.
    if [[ "/." != *"${path::1}"* ]]; then
        echo "~/$path"
    else
        echo "$path"
    fi
}

function _join {
    echo $*
}

conda_env_prefix=$(_short "$mydir/env")
conda_env_prefix_homed=$(_homed "$mydir/env")

function _mute {
    "${@}" &> /dev/null
}

function _log {
    echo -n "${FUNCNAME[${lognstack:-1}]}: ${@}" > /dev/stderr
}

function _cmd_before_update {
    local lognstack=$(( ${lognstack:-1} + 1))
    if [ -n "$force" ]; then
        set -- false  # Force 2nd (running) branch in the check below.
    fi
    if "${@}"; then
        _log -e "${notrun_msg:-already ok}"
        return 1
    else
        _log -en "${running_msg:-updating...}"
        if [ -n "${dry_run}" ]; then
            echo "${force:+(FORCE)}(DRY_RUN)" > /dev/stderr
            return 1 # (already run) when $dry_run
        fi
        return 0  # true, to run if-body updating stuff.
    fi
}

function _exists_before_update {
    local lognstack=$(( ${lognstack:-1} + 1))
    local check_file="$1"
    local notrun_msg="${notrun_msg:-already ok;
    'rm  $(_short $check_file)' and re-run, to effect change.}"
    local running_msg="${running_msg:-updating '$check_file'...}"
    _cmd_before_update [ -e "$check_file" ]
}

function _grep_before_update {
    local lognstack=$(( ${lognstack:-1} + 1))
    local check_file="$1" check_text="$2"
    local notrun_msg="${notrun_msg:-already ok;
    drop lines containing '$check_file::$check_text' and re-run, to effect change.}"
    local running_msg="${running_msg:-updating '$check_file'...}"
    _cmd_before_update _mute grep "$check_text" "$check_file"
}

function upd10_inputrc {
    local check_file="$HOME/.inputrc"
    if _exists_before_update "$check_file"; then
        cat >> "$check_file" <<- "EOF"
$include /etc/inputrc

## arrow up
"\e[A":history-search-backward
## arrow down
"\e[B":history-search-forward
"\C-x\C-r": re-read-init-file

EOF
        echo "UPDATED" > /dev/stderr
    fi
}

function upd20_bashrc_git_completions {
    local check_file="$HOME/.bashrc"
    if _grep_before_update "$check_file" "/usr/share/bash-completion/completions/git"; then
        cat >> "$check_file" <<- "EOF"

## UDS: Enable git completions
source /usr/share/bash-completion/completions/git

EOF
        echo "UPDATED" > /dev/stderr
    fi
}

function upd20_bashrc_options {
    local check_file="$HOME/.bashrc"
    if _grep_before_update "$check_file" "## UDS SHELL OPTIONS"; then
        cat >> "$check_file" <<- "EOF"

####
## UDS SHELL OPTIONS
## (contradicting options may lie above/below)
##
## Save all lines of a multiple-line command in the same history entry.
shopt -s cmdhist
## Save multi-line commands to the history with embedded newlines.
shopt -s lithist
## Save each and EVERY cmd to history
#  (not to loose it if terminal closed abruptly!)
PROMPT_COMMAND="history -a"
## Don't put duplicate lines or lines starting with space in the history.
## Ignore 1-char cmds
HISTIGNORE=?:?
## Stop BDAP from escaping variables when pressing TAB.
# It removes also some questionable autocompleton "intelligence"...
#  from: https://askubuntu.com/a/1175329/251379
shopt -u progcomp

EOF
        echo "UPDATED" > /dev/stderr
    fi
}

function upd20_bashrc_ustore_vars {
    local check_file="$HOME/.bashrc"
    if _grep_before_update "$check_file" "## UDS: BDAP Storage locations"; then
        cat >> "$check_file" <<- EOF

## UDS: BDAP Storage locations
export PEOS="/eos/jeodpp/data/projects/LEGENT"
export UEOS="/eos/jeodpp/home/users/${USER}"
export UTRANS="/eos/jeodpp/home/users/${USER}/transfer"
export PTRANS="/eos/jeodpp/data/projects/LEGENT/transfer"

EOF
        echo "UPDATED" > /dev/stderr
    fi
}

function upd20_bash_aliases {
    local check_file="$HOME/.bash_aliases"
    if _grep_before_update "$check_file" "## UDS: GIT ALIASES"; then
        cat >> "$check_file" <<- "EOF"

####
## UDS: GIT ALIASES
alias lg='git lg'
alias lgg='git lgg'
alias log='git log'
alias status='git status'
alias show='git show'
alias cm='git commit'
alias chk='git checkout'
alias ch-='git checkout -'
alias rs='git reset'
alias rss='git reset --soft'
alias rsh='git reset --hard'
alias rb='git rebase'
alias rbi='git rebase -i'
alias rba='git rebase --abort'
alias rbc='git rebase --continue'
alias chrp='git cherry-pick'
alias chrpa='git cherry-pick --abort'
alias chrpc='git cherry-pick --continue'
alias rem='git remote'
alias remv='git remote --verbose'
alias rema='git remote add'
alias remu='git remote set-url'
alias remr='git remote rename'

_git_completion_cmd="complete -o bashdefault -o default -o nospace -F"

function __git_wrap__git_log { __git_func_wrap _git_log; }
function __git_wrap__git_status { __git_func_wrap _git_status; }
function __git_wrap__git_show { __git_func_wrap _git_show; }
function __git_wrap__git_commit { __git_func_wrap _git_commit; }
function __git_wrap__git_checkout { __git_func_wrap _git_checkout; }
function __git_wrap__git_reset { __git_func_wrap _git_reset; }
function __git_wrap__git_rebase { __git_func_wrap _git_rebase; }
function __git_wrap__git_cherry_pick { __git_func_wrap _git_cherry_pick; }
function __git_wrap__git_remote { __git_func_wrap _git_remote; }

$_git_completion_cmd __git_wrap__git_log        log
$_git_completion_cmd __git_wrap__git_log        lg
$_git_completion_cmd __git_wrap__git_log        lgg

$_git_completion_cmd __git_wrap__git_show       show

$_git_completion_cmd __git_wrap__git_reset      rs
$_git_completion_cmd __git_wrap__git_reset      rss
$_git_completion_cmd __git_wrap__git_reset      rsh

$_git_completion_cmd __git_wrap__git_checkout   ch
$_git_completion_cmd __git_wrap__git_checkout   ch-

$_git_completion_cmd __git_wrap__git_rebase     rb
$_git_completion_cmd __git_wrap__git_rebase     rbi
$_git_completion_cmd __git_wrap__git_rebase     rbc
$_git_completion_cmd __git_wrap__git_rebase     rba

$_git_completion_cmd __git_wrap__git_cherry_pick        chrp
$_git_completion_cmd __git_wrap__git_cherry_pick        chrpc
$_git_completion_cmd __git_wrap__git_cherry_pick        chrpa

$_git_completion_cmd __git_wrap__git_cherry_remote      rem
$_git_completion_cmd __git_wrap__git_cherry_remote      remv
$_git_completion_cmd __git_wrap__git_cherry_remote      rema
$_git_completion_cmd __git_wrap__git_cherry_remote      remu
$_git_completion_cmd __git_wrap__git_cherry_remote      remr

EOF
        echo "UPDATED" > /dev/stderr
    fi
}

function upd30_git_user_name {
    local running_msg="+++Enter your git AUTHOR-NAME: "
    if _cmd_before_update _mute git config --global --get user.name; then

        read -e inp
        git config --global user.name "$inp"
        echo "UPDATED" > /dev/stderr
    fi
}

function upd30_git_user_email {
    local running_msg="+++Enter your git AUTHOR-EMAIL: "
    if _cmd_before_update _mute git config --global --get user.email; then
        read -e inp
        git config --global user.email "$inp"
        _log "UPDATED"
    fi
}

function upd20_git_aliases {
    if _mute git config --global --get alias.lg; then
        _log "already ok"
    else
        _log -n "updating..."
        git config --global credential.helper store

        git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
        git config --global alias.lgg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --all"
        git config --global alias.cm    'commit'
        git config --global alias.chk   'checkout'
        git config --global alias.ch-   'checkout -'
        git config --global alias.rs    'reset'
        git config --global alias.rss   'reset --soft'
        git config --global alias.rsh   'reset --hard'
        git config --global alias.rb    'rebase'
        git config --global alias.rbi   'rebase -i'
        git config --global alias.rba   'rebase --abort'
        git config --global alias.rbc   'rebase --continue'
        git config --global alias.chrp  'cherry-pick'
        git config --global alias.chrpa 'cherry-pick --abort'
        git config --global alias.chrpc 'cherry-pick --continue'
        git config --global alias.rem   'remote'
        git config --global alias.remv  'remote --verbose'
        git config --global alias.rema  'remote add'
        git config --global alias.remu  'remote set-url'
        git config --global alias.remr  'remote rename'

        _log "UPDATED"
    fi
}

function upd20_ipython_ustore_vars {
    local check_file="$HOME/.ipython/profile_default/startup/50-ustor-vars.py"
    if _exists_before_update "$check_file"; then
        mkdir -p  $(dirname "$check_file")
        cat > "$check_file" <<- EOF
## BDAP Storage locations
PEOS="/eos/jeodpp/data/projects/LEGENT"
UEOS="/eos/jeodpp/home/users/${USER}"
UTRANS="/eos/jeodpp/home/users/${USER}/transfer"
PTRANS="/eos/jeodpp/data/projects/LEGENT/transfer"

EOF
        echo "UPDATED" > /dev/stderr
    fi
}

function upd30_bashrc_condarc {
    local check_file="$HOME/.condarc"
    local check_text="## UDS CONFIGS END"
    if _grep_before_update "$check_file" "$check_text"; then
        ## Header for new props appended (apart from those already there).
        local check_text2="## UDS CONFIGS START"
        if _mute grep -v "$check_text2" "$check_file"; then
            echo -e "\n$check_text2" >> "$check_file"
        fi

        # Strict-priority for when using conda-forge, to speedup
        # eg for `conda-bash-completion` package`,
        # see https://conda-forge.org/docs/user/tipsandtricks.html#how-to-fix-it.
        conda config --set channel_priority strict
        # Suggested by BDAP docs:
        # https://jeodpp.jrc.ec.europa.eu/apps/gitlab/for-everyone/documentation/-/wikis/Jeodpp_services/Jeodesk/JEO-Desk-20-user-manual#conda-use-example-creation-of-an-environment-with-python-35-and-numpy-accessible-via-jupyterlab
        conda config --set auto_activate_base False
        # To reduce bash-prompt width, from:
        # https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#id3
        conda config --set env_prompt '({name})'
        # Stop "conda outdated" message (is `conda env update` faster?).
        conda config --set auto_update_conda False

        echo "$check_text" >> "$check_file"
    fi
}

function upd40_bashrc_conda_init {
    local check_file="$HOME/.bashrc"
    if _grep_before_update "$check_file" ">>> conda initialize >>>"; then
        ## Will print a message to logout & re-login.
        conda init bash

        if [ -n "$keep_going" ]; then
            _log "Would STOP to logout + re-login if not KEEP_GOING."
        else
            exit 0
        fi
    fi
}

## This function must be the 1st to run after all `upd_xxx` actions.
#  The last command of this action, activating conda-env,
#  will fail if not having re-logged in after `conda init bash`.
function upd50_create_conda_env {
    local check_file="$conda_env_prefix"
    local running_msg="should take ~5min to create a conda-env@$conda_env_prefix..."
    local notrun_msg="already created;
        DELETE it with 'conda env remove $conda_env_prefix_homed' and re-run, to effect change.

        Reminder for how to (re-)activate your environment: conda activate $conda_env_prefix_homed"
    if _exists_before_update "$check_file"; then
        conda env create -p "$conda_env_prefix" -f environment.yml
    fi
}

function upd60_conda_env_vars {
    ## Setup any conda-env variables:
    #  - make PATH & PYTHONPATH pointing to project-root,
    #    to be able run & import scripts from anywhere.
    local check_file=$( _short "$conda_env_prefix/etc/conda/activate.d/uds_vars.sh")
    if _exists_before_update "$check_file"; then
        mkdir -p $(dirname "$check_file")
        cat > "$check_file" <<- EOF
#!/bin/sh

export _UDS_OLD_PATH="\$PATH"
export _UDS_OLD_PYTHONPATH="\$PYTHONPATH"
export PATH="$mydir:\$PATH"
export PYTHONPATH="$mydir:\$PYTHONPATH"
EOF
        check_file=$( _short "$conda_env_prefix/etc/conda/deactivate.d/uds_vars.sh")
        mkdir -p $(dirname "$check_file")
        cat > "$check_file" <<- EOF
#!/bin/sh

export PATH="\$_UDS_OLD_PATH"
export PYTHONPATH="\$_UDS_OLD_PYTHONPATH"
unset _UDS_OLD_PATH
unset _UDS_OLD_PYTHONPATH
EOF
        echo "UPDATED
        RE-ACTIVATE YOUR ENVIRONMENT with: conda activate $conda_env_prefix_homed" > /dev/stderr
    fi
}

function _list_all_actions {
    declare -F|grep -oE ' [a-zA-Z]\w+$' | sort
}

function all {
    for cmd in $(_list_all_actions); do
        if [ "$cmd" != "${FUNCNAME[0]}" ]; then
            $cmd
        fi
    done
}

## Trick to search if NOT help- asked
# from: https://superuser.com/a/1002826/227922
if [[ "${@#-h}" != "$@" || "${@#--help}" != "$@" ]]; then
    _HELP
else
    let nargs="$#"
    if [[ "${@#-k}" != "$@" ]]; then
        keep_going=1
        set -- "${@#-k}"
        ((nargs-=1))
    fi
    if [[ "${@#-n}" != "$@" ]]; then
        dry_run=1
        set -- "${@#-n}"
        ((nargs-=1))
    fi
    if [[ "${@#-f}" != "$@" ]]; then
        force=1
        set -- "${@#-f}"
        ((nargs-=1))
    fi

    if [ -z "$keep_going" ]; then
        set -e
    fi

    if [ "$nargs" -eq 0 ]; then
        echo "Running all actions${dry_run:+(DRY_RUN)}${keep_going:+(KEEP_GOING)}${force:+(FORCE)}..." > /dev/stderr
        all
    else
        echo "Running x$nargs actions: '${@}'" > /dev/stderr
        ## execute the asked action.
        for cmd in "${@}"; do
            $cmd
        done
    fi
fi
